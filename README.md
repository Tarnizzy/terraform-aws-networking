# Sandbox AWS VPC Networking 

This module creates Networking Resources (vpc, subnets, vpc flow logs, nat gateway, internet gateway, and secondary cidr block) for our sandbox environment.

## Deploys VPC Networking on AWS Environment

This Terraform module deploys Network resources.

> Note: For the module to work, it needs several required variables corresponding to existing resources in AWS. Please refer to the variable section for the list of required variables.

## Getting started

The following example contains the bare minimum options to be configured for Security Group deployment. 


First, create a `version.tf` file.

Next, copy the code below.

```hcl
//update this block to conform to your organisation and workspace.
terraform {
  backend "remote" {
    hostname     = "terraform.safaricom.net"
    organization = "safaricom-sandbox"

    workspaces {
      name = "chapterboost"
    }
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 4.0"
    }
  }
}
```


Next, create `main.tf` file

Coy the code below:

```hcl
// Remember to update the version to your liking.

```

Next, create `variables.tf` file

Coy the code below:

```hcl

```

Finally, push your code to gitlab, terraform enterprise will pick from there.

## Contributing

This module is the work of the below contributors. We appreciate your help!

 1. Noah Makau

## License

[MIT](LICENSE)


<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.2.7 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 4.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | >= 4.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_default_security_group.default](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/default_security_group) | resource |
| [aws_eip.elastic_ip](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eip) | resource |
| [aws_internet_gateway.igw](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/internet_gateway) | resource |
| [aws_nat_gateway.private_ngw](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/nat_gateway) | resource |
| [aws_route_table.eks_nodes_rtb](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table) | resource |
| [aws_route_table.private_data_rtb](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table) | resource |
| [aws_route_table.private_eks_rtb](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table) | resource |
| [aws_route_table.public_rtb](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table) | resource |
| [aws_route_table_association.eks_nodes_rtb_ass](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association) | resource |
| [aws_route_table_association.private_data_rtb_ass](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association) | resource |
| [aws_route_table_association.private_eks_rtb_ass](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association) | resource |
| [aws_route_table_association.public_rtb_ass](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association) | resource |
| [aws_subnet.eks_nodes](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet) | resource |
| [aws_subnet.private_data](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet) | resource |
| [aws_subnet.private_eks](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet) | resource |
| [aws_subnet.public](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet) | resource |
| [aws_vpc.vpc](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc) | resource |
| [aws_vpc_ipv4_cidr_block_association.secondary](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_ipv4_cidr_block_association) | resource |
| [aws_availability_zones.available](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/availability_zones) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_connectivity_type"></a> [connectivity\_type](#input\_connectivity\_type) | The connectivity type for the nat gateway | `string` | `"public"` | no |
| <a name="input_eks_nodes_subnets_cidr"></a> [eks\_nodes\_subnets\_cidr](#input\_eks\_nodes\_subnets\_cidr) | CIDR Blocks to use for EKS nodes subnets | `list(string)` | n/a | yes |
| <a name="input_enable_dns_hostnames"></a> [enable\_dns\_hostnames](#input\_enable\_dns\_hostnames) | Should be true to enable DNS hostnames in the VPC | `bool` | `true` | no |
| <a name="input_enable_dns_support"></a> [enable\_dns\_support](#input\_enable\_dns\_support) | Should be true to enable DNS support in the VPC | `bool` | `true` | no |
| <a name="input_primary_cidr"></a> [primary\_cidr](#input\_primary\_cidr) | The cidr block value for the project vpc | `string` | n/a | yes |
| <a name="input_private_data_subnets_cidr"></a> [private\_data\_subnets\_cidr](#input\_private\_data\_subnets\_cidr) | CIDR Blocks to use for private data subnets | `list(string)` | n/a | yes |
| <a name="input_private_eks_subnets_cidr"></a> [private\_eks\_subnets\_cidr](#input\_private\_eks\_subnets\_cidr) | CIDR Blocks to use for private application/EKS subnets | `list(string)` | n/a | yes |
| <a name="input_project"></a> [project](#input\_project) | Name of project | `string` | n/a | yes |
| <a name="input_public_map_public_ip_on_launch"></a> [public\_map\_public\_ip\_on\_launch](#input\_public\_map\_public\_ip\_on\_launch) | Are we enabling public ip on public subnets | `bool` | `true` | no |
| <a name="input_public_subnets_cidr"></a> [public\_subnets\_cidr](#input\_public\_subnets\_cidr) | CIDR Blocks to use for public subnets | `list(string)` | n/a | yes |
| <a name="input_region"></a> [region](#input\_region) | Region to create  resources | `string` | n/a | yes |
| <a name="input_rtb_cidr"></a> [rtb\_cidr](#input\_rtb\_cidr) | The cidr block value for routetables | `string` | n/a | yes |
| <a name="input_secondary_cidr"></a> [secondary\_cidr](#input\_secondary\_cidr) | List of secondary CIDR blocks to associate with the VPC to extend the IP Address pool | `string` | n/a | yes |
| <a name="input_tags"></a> [tags](#input\_tags) | tags to be used on project | `map(any)` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_data_subnets_id"></a> [data\_subnets\_id](#output\_data\_subnets\_id) | The IDs to the data subnets created |
| <a name="output_eks_nodes_subnets_id"></a> [eks\_nodes\_subnets\_id](#output\_eks\_nodes\_subnets\_id) | The IDs to the eks nodes subnets created |
| <a name="output_igw_id"></a> [igw\_id](#output\_igw\_id) | The ID to Internet Gateway |
| <a name="output_private_ngw_id"></a> [private\_ngw\_id](#output\_private\_ngw\_id) | The ID to the private nat gateway created |
| <a name="output_private_subnets_id"></a> [private\_subnets\_id](#output\_private\_subnets\_id) | The IDs to the private eks/application subnets created |
| <a name="output_public_subnets_id"></a> [public\_subnets\_id](#output\_public\_subnets\_id) | The IDs to the public subnets created |
| <a name="output_vpc_id"></a> [vpc\_id](#output\_vpc\_id) | The ID to the VPC Created |
<!-- END_TF_DOCS -->
